HFCC Scholarship Search Transfer
--------------------------------

Project Lead: Micah Webner <micah@hfcc.edu>

This Drupal module executes a one-time data transfer to migrate the
Henry Ford Community College Foundation Scholarship Search from a
legacy database to Drupal nodes. As is, this module is unsuitable for
any other purpose.
